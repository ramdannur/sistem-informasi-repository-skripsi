<x-guest-layout>
    <div class="row">
        <div class="col-12 col-lg-8">
            <div class="card card-body">
                <h3>{{ $document->title }}</h3>
                Submitted on {{ $document->publish_year }}
                <br>
                <br>
                <br>
                <br>
                <br>
                {{ $document->member->prodi->fakultas->name }} | {{ $document->member->prodi->name }}
                <br>
                <br>
                {{ $document->member->nim }}
                <br>
                {{ $document->member->firstname }} {{ $document->member->lastname }}
                <br>
                <br>
                @auth
                <div class="text-left">
                    <a href="{{ URL::to('/uploads') . '/' . $document->attachment }}">
                        <x-button class="btn btn-primary">
                            Lihat selengkapnya
                        </x-button>
                    </a>
                </div>
                @endauth
            </div>
        </div>

        <div class="col-12 col-lg-4">
            <div class="mb-3">
                <a href="#"><img src="{{ URL::to('/uploads') . '/' . $document->image }}" class="img-fluid rounded" alt=""></a>
            </div>
        </div>

        <div class="col-12">
            <h3>ABSTRAKSI</h3>
            <p>{{ $document->abstract }}</p>
        </div>
    </div>

</x-guest-layout>