<x-guest-layout>
        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

		 <!-- Search field -->
         <div class="card">
        <div class="card-body">
            <form class="sk-form">
                <div class="d-sm-flex">
                    <div class="form-group-feedback form-group-feedback-left flex-grow-1 mb-3 mb-sm-0">
                        <input type="text" class="form-control form-control-lg sk-search" name="q" placeholder="Search" value="{{ Request::get('q') }}">
                        <div class="form-control-feedback form-control-feedback-lg">
                            <i class="icon-search4 text-muted"></i>
                        </div>
                    </div>

                    <div class="ml-sm-3">
                        <button type="submit" class="btn btn-primary btn-lg w-100 w-sm-auto sk-btn">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /search field -->


    <!-- Search results -->
    <div class="row">
        <div class="col-12 col-lg-8">
            <div class="card card-body sk-body">

                <ul class="media-list mb-3 sk-contents">
                    @foreach($documents as $document)
                    <li class="media sk-content">
                        <div class="media-body">
                            <h6 class="media-title"><a href="{{ route('site.document.detail', ['id' => $document->id]) }}">{{ $document->title }}</a></h6>
                            {{ $document->abstract }}
                        </div>
                    </li>
                    @endforeach
                </ul>

                @if ($documents->lastPage() > 1)
                <ul class="pagination pagination-flat align-self-center flex-wrap my-2">
                    <li class="page-item {{ ($documents->currentPage() == 1) ? ' disabled' : '' }}">
                        <a href="{{ $documents->url(1) }}" class="page-link"><i class="icon-arrow-left8"></i></a>
                    </li>
                    @for ($i = 1; $i <= $documents->lastPage(); $i++)
                        <li class="page-item {{ ($documents->currentPage() == $i) ? ' active' : '' }}">
                            <a href="{{ $documents->url($i) }}" class="page-link">{{ $i }}</a>
                        </li>
                    @endfor
                    <li class="page-item {{ ($documents->currentPage() == $documents->lastPage()) ? ' disabled' : '' }}">
                        <a href="{{ $documents->url($documents->currentPage()+1) }}" class="page-link"><i class="icon-arrow-right8"></i></a>
                    </li>
                </ul>
                @endif
            </div>
        </div>

        <div class="col-12 col-lg-4">
            <div class="card card-body">
                <div class="mb-3">
                    <a href="#"><img src="{{ URL::to('/') }}/images/placeholder.png" class="img-fluid rounded" alt=""></a>
                </div>

                <h5 class="font-weight-semibold">Sistem Informasi Skripsi</h5>
                <p class="mb-3">Merupakan sistem yang diharapkan dapat membantu Fakultas Ilmu Komputer dan Teknologi Informasi (FKTI) untuk mengatur pengarsipan data-data skripsi yang telah dikerjakan oleh para mahasiswanya menjadi lebih rapi, aman dan mudah untuk dikelola</p>
            </div>
        </div>
    </div>
    <!-- /search results -->

        
</x-guest-layout>