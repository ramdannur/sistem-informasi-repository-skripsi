<x-app-layout>
<div class="card">
	<div class="card-header">
		<h5 class="card-title">Profile</h5>
	</div>
	<div class="card-body">
		<div class="media-body">
			<div class="media-title font-weight-semibold">Name</div>
			<span class="text-muted font-size-sm">{{ Auth::user()->name }}</span>
		</div>
		<br>
		<div class="media-body">
			<div class="media-title font-weight-semibold">Email </div>
			<span class="text-muted font-size-sm">{{ Auth::user()->email }}</span>
		</div>
		<br>
		<div class="media-body">
			<div class="media-title font-weight-semibold">Role </div>
			<span class="text-muted font-size-sm">{{ Auth::user()->role->name }}</span>
		</div>
	</div>
</x-app-layout>
