<x-app-layout>
<div class="card">
						<div class="card-header">
							<h5 class="card-title">List Member</h5>
						</div>

						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Email</th>
										<th>Birth Date</th>
										<th>Tahun Angkatan</th>
										<th>Prodi</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach($members as $value)
									<tr>
										<td>1</td>
										<td>{{ $value->firstname }}</td>
										<td>{{ $value->lastname }}</td>
										<td>{{ $value->user->email }}</td>
										<td>{{ $value->birthdate }}</td>
										<td>{{ $value->tahun_angkatan }}</td>
										<td>{{ $value->prodi->name }}</td>
									</tr>
                                    @endforeach
								</tbody>
							</table>
						</div>

                        @if ($members->lastPage() > 1)
                        <ul class="pagination pagination-flat align-self-center flex-wrap my-2">
                            <li class="page-item {{ ($members->currentPage() == 1) ? ' disabled' : '' }}">
                                <a href="{{ $members->url(1) }}" class="page-link"><i class="icon-arrow-left8"></i></a>
                            </li>
                            @for ($i = 1; $i <= $members->lastPage(); $i++)
                                <li class="page-item {{ ($members->currentPage() == $i) ? ' active' : '' }}">
                                    <a href="{{ $members->url($i) }}" class="page-link">{{ $i }}</a>
                                </li>
                            @endfor
                            <li class="page-item {{ ($members->currentPage() == $members->lastPage()) ? ' disabled' : '' }}">
                                <a href="{{ $members->url($members->currentPage()+1) }}" class="page-link"><i class="icon-arrow-right8"></i></a>
                            </li>
                        </ul>
                        @endif
					</div>
</x-app-layout>
