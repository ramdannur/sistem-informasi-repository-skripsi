<x-guest-layout>
        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <div class="card">
				<div class="card-header">
					<h5 class="card-title">Sign In</h5>
				</div>

				<div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
						@csrf

						<div class="form-group">
							<x-label for="email" :value="__('Email')" />
							<x-input id="email" class="form-control" type="email" name="email" :value="old('email')" required />
						</div>

						<div class="form-group">
							<x-label for="password" :value="__('Password')" />
							<x-input id="password" class="form-control" type="password" name="password" required autocomplete="current-password"/>
						</div>
						
                        <div class="block mt-4">
                            <label for="remember_me" class="inline-flex items-center">
                                <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                                <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                            </label>
                        </div>

						<div class="text-right">
							<x-button class="btn btn-primary">
                            {{ __('Log in') }}<i class="icon-paperplane ml-2"></i>
							</x-button>
						</div>
					</form>
                    Belum punya akun? <a href="{{ route('register') }}">Daftar disini</a>
				</div>
			</div>
</x-guest-layout>
