<x-guest-layout>
        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

		<!-- Basic layout-->
		<div class="card">
			<div class="card-header">
				<h5 class="card-title">Register</h5>
			</div>

			<div class="card-body">
				<form method="POST" action="{{ route('register') }}">
					@csrf
					<div class="row form-group">
						<div class="col-lg-6">
							<x-label for="firstname" :value="__('First Name')" />
							<x-input id="firstname" class="form-control" type="text" name="firstname" :value="old('firstname')" required autofocus />
						</div>

						<div class="col-lg-6">
						<x-label for="lastname" :value="__('Last Name')" />
							<x-input id="lastname" class="form-control" type="text" name="lastname" :value="old('lastname')" required autofocus />
						</div>

					</div>

					<div class="form-group">
						<x-label for="name" :value="__('Prodi')" />
						<select class="form-control" name="prodi">
							<option disabled>[-- Pilih Prodi --]</option>
						    @foreach($prodi as $value)
							<option value="{{ $value->id }}">{{ $value->name }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<x-label for="nim" :value="__('NIM')" />
						<x-input id="nim" class="form-control" type="text" name="nim" :value="old('nim')" required />
					</div>

					<div class="form-group">
						<x-label for="email" :value="__('Email')" />
						<x-input id="email" class="form-control" type="email" name="email" :value="old('email')" required />
					</div>

					<div class="form-group">
						<x-label for="password" :value="__('Password')" />
						<x-input id="password" class="form-control" type="password" name="password" required autocomplete="new-password"/>
					</div>

					<div class="form-group">
						<x-label for="password_confirmation" :value="__('Confirm Password')" />
						<x-input id="password_confirmation" class="form-control" type="password" name="password_confirmation" required/>
					</div>

					<div class="flex items-center justify-end mt-4">
						<a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
							{{ __('Already registered?') }}
						</a>
					</div>

					<div class="text-right">
						<x-button class="btn btn-primary">
							{{ __('Register') }}<i class="icon-paperplane ml-2"></i>
						</x-button>
					</div>
				</form>
			</div>
		</div>
		<!-- /basic layout -->

        
</x-guest-layout>