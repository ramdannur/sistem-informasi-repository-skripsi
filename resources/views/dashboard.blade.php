<x-app-layout>
<div class="row">
    <div class="col-xl-8 text-center img-center">
    <div class="sidebar-section sidebar-section-body user-menu-vertical text-center">
            <div class="card-img-actions d-inline-block">
                <svg id="Line" enable-background="new 0 0 96 96" height="150" viewBox="0 0 96 96" width="150" xmlns="http://www.w3.org/2000/svg"><path d="m3 51h64v-6c0-1.105-.895-2-2-2h-60c-1.105 0-2 .895-2 2z" fill="#ef6662"/><path d="m5 93h60c1.105 0 2-.895 2-2v-40h-64v40c0 1.105.895 2 2 2z" fill="#e6e5e4"/><path d="m13 39h-10c-.552 0-1-.447-1-1s.448-1 1-1h9.279l2.772-8.316c.128-.384.475-.652.878-.681.398-.028.784.188.965.55l2.96 5.921 3.206-8.815c.144-.396.52-.659.94-.659h.014c.425.006.8.28.935.684l2.772 8.316h8.279c.552 0 1 .447 1 1s-.448 1-1 1h-9c-.431 0-.813-.275-.949-.684l-2.092-6.277-3.019 8.303c-.137.375-.483.634-.882.656-.397.026-.773-.193-.952-.551l-2.92-5.841-2.236 6.71c-.138.409-.519.684-.95.684z" fill="#6ebaed"/><path d="m86.98 36.026c-.459 11.834-8.831 21.64-19.98 24.274v-15.3c0-1.105-.895-2-2-2h-28.74c-.82-2.52-1.26-5.21-1.26-8 0-14.016 11.089-25.441 24.973-25.98.562-.022 1.027.443 1.027 1.006v23.974c0 .552.448 1 1 1h23.975c.562 0 1.027.465 1.005 1.026z" fill="#c9c7c6"/><path d="m93 29c0-14.371-11.629-26-26-26v25c0 .552.448 1 1 1z" fill="#f2be07"/><g fill="#c9c7c6"><path d="m10 51h2v42h-2z"/><path d="m17 51h2v42h-2z"/><path d="m24 51h2v42h-2z"/><path d="m31 51h2v42h-2z"/><path d="m38 51h2v42h-2z"/><path d="m45 51h2v42h-2z"/><path d="m52 51h2v42h-2z"/><path d="m59 51h2v42h-2z"/><path d="m3 57h64v2h-64z"/><path d="m3 64h64v2h-64z"/><path d="m3 71h64v2h-64z"/><path d="m3 78h64v2h-64z"/><path d="m3 85h64v2h-64z"/></g><path d="m69 93h-8v-13c0-1.105.895-2 2-2h6z" fill="#3ba1e3"/><path d="m61 93h-8v-8c0-1.105.895-2 2-2h6z" fill="#6ebaed"/><path d="m77 93h-8v-23c0-1.105.895-2 2-2h6z" fill="#6ebaed"/><path d="m85 93h-8v-29c0-1.105.895-2 2-2h4c1.105 0 2 .895 2 2z" fill="#3ba1e3"/><path d="m93 93h-8v-19h6c1.105 0 2 .895 2 2z" fill="#6ebaed"/><path d="m5.999 31.991c-.156 0-.315-.036-.463-.114-.489-.257-.678-.861-.421-1.35l11-20.982c.256-.49.86-.68 1.35-.422.489.257.678.861.421 1.35l-11 20.982c-.179.342-.527.536-.887.536z" fill="#316596"/><path d="m28 20.992c-.217 0-.435-.07-.618-.214-.434-.342-.509-.971-.168-1.404l11-13.984c.342-.435.971-.507 1.404-.168.434.342.509.971.168 1.404l-11 13.984c-.197.251-.491.382-.786.382z" fill="#316596"/><path d="m27.984 21c-.24 0-.481-.086-.673-.261l-10.97-10c-.408-.372-.438-1.005-.065-1.413.373-.406 1.005-.437 1.413-.065l10.97 10c.408.372.438 1.005.065 1.413-.198.216-.468.326-.74.326z" fill="#316596"/><circle cx="6" cy="31" fill="#6ebaed" r="3"/><circle cx="17" cy="10" fill="#6ebaed" r="3"/><circle cx="28" cy="20" fill="#6ebaed" r="3"/><circle cx="39" cy="6" fill="#6ebaed" r="3"/><path d="m73 24c-.256 0-.512-.098-.707-.293-.391-.391-.391-1.023 0-1.414l8-8c.391-.391 1.023-.391 1.414 0s.391 1.023 0 1.414l-8 8c-.195.195-.451.293-.707.293z" fill="#316596"/><circle cx="74" cy="16" fill="#316596" r="1"/><circle cx="80" cy="22" fill="#316596" r="1"/><path d="m51 28h-2c-1.103 0-2-.897-2-2s.897-2 2-2h2c1.103 0 2 .897 2 2 0 .553.448 1 1 1s1-.447 1-1c0-2.206-1.794-4-4-4v-1c0-.553-.448-1-1-1s-1 .447-1 1v1c-2.206 0-4 1.794-4 4s1.794 4 4 4h2c1.103 0 2 .897 2 2s-.897 2-2 2h-2c-1.103 0-2-.897-2-2 0-.553-.448-1-1-1s-1 .447-1 1c0 2.206 1.794 4 4 4v1c0 .553.448 1 1 1s1-.447 1-1v-1c2.206 0 4-1.794 4-4s-1.794-4-4-4z" fill="#316596"/><circle cx="31" cy="47" fill="#e05551" r="1"/><circle cx="35" cy="47" fill="#e05551" r="1"/><circle cx="39" cy="47" fill="#e05551" r="1"/></svg>
            </div>

            <div class="sidebar-resize-hide position-relative mt-2">
                    <h6 class="font-weight-semibold">Welcome, {{ Auth::user()->name }}</h6>
                    <span class="d-block text-muted">{{ Auth::user()->email }}</span>
            </div>
        </div>
    </div>

    <div class="col-xl-4">

        <!-- Progress counters -->
        <div class="row">
            <div class="col-sm-6">

                <!-- Available hours -->
                <div class="card text-center">
                    <div class="card-body">

                        <!-- Progress counter -->
                        <div class="svg-center position-relative" id="hours-available-progress">
                        <svg width="76" height="76"><g transform="translate(38,38)"><path class="d3-progress-background" d="M0,38A38,38 0 1,1 0,-38A38,38 0 1,1 0,38M0,36A36,36 0 1,0 0,-36A36,36 0 1,0 0,36Z" style="fill: rgb(240, 98, 146); opacity: 1;"></path><path class="d3-progress-foreground" filter="url(#blur)" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(240, 98, 146); stroke: rgb(240, 98, 146);"></path><path class="d3-progress-front" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.38342799370878,16.179613079472677L-32.57377388877674,15.328054496342538A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(240, 98, 146); fill-opacity: 1;"></path></g></svg>
                        <h2 class="pt-1 mt-2 mb-1">{{ $total_member }}</h2><i class="icon-checkmark3 text-pink counter-icon" style="top: 22px"></i>
                        <div>Member</div>
                        <div class="font-size-sm text-muted mb-3">terdaftar</div></div>
                        <!-- /progress counter -->

                    </div>
                </div>
                <!-- /available hours -->

            </div>

            <div class="col-sm-6">

                <!-- Productivity goal -->
                <div class="card text-center">
                    <div class="card-body">

                        <!-- Progress counter -->
                        <div class="svg-center position-relative" id="goal-progress">
                        <svg width="76" height="76"><g transform="translate(38,38)"><path class="d3-progress-background" d="M0,38A38,38 0 1,1 0,-38A38,38 0 1,1 0,38M0,36A36,36 0 1,0 0,-36A36,36 0 1,0 0,36Z" style="fill: rgb(92, 107, 192); fill-opacity: 1;"></path><path class="d3-progress-foreground" filter="url(#blur)" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.3834279937087,-16.179613079472855L-32.573773888776664,-15.328054496342704A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(92, 107, 192); stroke: rgb(92, 107, 192);"></path><path class="d3-progress-front" d="M2.326828918379971e-15,-38A38,38 0 1,1 -34.3834279937087,-16.179613079472855L-32.573773888776664,-15.328054496342704A36,36 0 1,0 2.204364238465236e-15,-36Z" style="fill: rgb(92, 107, 192); fill-opacity: 1;"></path></g></svg>
                        <h2 class="pt-1 mt-2 mb-1">{{ $total_document }}</h2><i class="icon-checkmark3 text-indigo counter-icon" style="top: 22px"></i>
                        <div>Skripsi</div>
                        <div class="font-size-sm text-muted mb-3">terupload</div></div>
                        <!-- /progress counter -->

                    </div>
                </div>
                <!-- /productivity goal -->

            </div>
        </div>
        <!-- /progress counters -->

    </div>
</div>
</x-app-layout>
