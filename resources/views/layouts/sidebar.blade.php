
        <!-- Main sidebar -->
        <div class="sidebar sidebar-light sidebar-main sidebar-expand-lg">

            <!-- Sidebar content -->
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-section sidebar-user my-1">
                    <div class="sidebar-section-body">
                        <div class="media">
                            <a href="#" class="mr-3">
                                <img src="{{ URL::to('/') }}/images/placeholder-person.jpeg" class="rounded-circle" alt="">
                            </a>

                            <div class="media-body">
                                <div class="font-weight-semibold">{{ Auth::user()->name }}</div>
                            </div>

                            <div class="ml-3 align-self-center">
                                <button type="button" class="btn btn-outline-light text-body border-transparent btn-icon rounded-pill btn-sm sidebar-control sidebar-main-resize d-none d-lg-inline-flex">
                                    <i class="icon-transmission"></i>
                                </button>

                                <button type="button" class="btn btn-outline-light text-body border-transparent btn-icon rounded-pill btn-sm sidebar-mobile-main-toggle d-lg-none">
                                    <i class="icon-cross2"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->


                <!-- Main navigation -->
                <div class="sidebar-section">
                    <ul class="nav nav-sidebar" data-nav-type="accordion">

                        <!-- Main -->
                        <li class="nav-item">
                            <a href="{{ route('dashboard') }}" class="nav-link active">
                                <i class="icon-home4"></i>
                                <span>
                                    Dashboard
                                </span>
                            </a>
                        </li>

                        <?php
                            if (Auth::user()->role_id == 1) {
                                ?>
                                    <li class="nav-item">
                                        <a href="{{ route('admin.member.index') }}" class="nav-link">
                                            <i class="icon-list-unordered"></i>
                                            <span>
                                                List Member
                                            </span>
                                        </a>
                                    </li>
                                <?php
                            } 
                            
                            ?>
                                <li class="nav-item">
                                    <a href="{{ route('member.document.index') }}" class="nav-link">
                                        <i class="icon-list-unordered"></i>
                                        <span>
                                            List Document
                                        </span>
                                    </a>
                                </li>
                            <?php

                            if(Auth::user()->role_id == 2){
                            
                            }
                        ?>
                        <!-- /main -->
                    </ul>
                </div>
                <!-- /main navigation -->

            </div>
            <!-- /sidebar content -->
            
        </div>
        <!-- /main sidebar -->