<!-- Main navbar -->
<div class="navbar navbar-expand-lg navbar-light navbar-static">
		<div class="d-flex flex-1 d-lg-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-paragraph-justify3"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-transmission"></i>
			</button>
		</div>

		<div class="navbar-brand text-center text-lg-left">
			<a href="index.html" class="d-inline-block">
				<img src="{{ URL::to('/') }}/images/logo_light.png" class="d-none d-sm-block" alt="">
				<img src="{{ URL::to('/') }}/images/logo_icon_light.png" class="d-sm-none" alt="">
			</a>
		</div>

		<div class="collapse navbar-collapse order-2 order-lg-1" id="navbar-mobile">
		</div>

		<ul class="navbar-nav flex-row order-1 order-lg-2 flex-1 flex-lg-0 justify-content-end align-items-center">
			<li class="nav-item nav-item-dropdown-lg dropdown dropdown-user h-100">
				<a href="#" class="navbar-nav-link navbar-nav-link-toggler dropdown-toggle d-inline-flex align-items-center h-100" data-toggle="dropdown">
					<img src="{{ URL::to('/') }}/images/placeholder-person.jpeg" class="rounded-pill" height="34" alt="">
					<span class="d-none d-lg-inline-block ml-2">{{ Auth::user()->name }}</span>
				</a>
				<div class="dropdown-menu dropdown-menu-right">

				<a href="{{ route('profile.index') }}" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
                    <div class="dropdown-divider"></div>
					<form method="POST" action="{{ route('logout') }}">
						@csrf
						<a :href="route('logout')"
								onclick="event.preventDefault();
											this.closest('form').submit();" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
					</form>
				</div>
			</li>
		</ul>
	</div>
	<!-- /main navbar -->