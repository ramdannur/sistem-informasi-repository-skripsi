<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('css/all.min.css') }}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="{{ asset('js/jquery.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="{{ asset('js/app.js') }}"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-expand-lg navbar-light navbar-static">
		<div class="d-flex flex-1 d-lg-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-paragraph-justify3"></i>
			</button>
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-transmission"></i>
			</button>
		</div>

		<div class="navbar-brand text-center text-lg-left">
			<a href="index.html" class="d-inline-block">
				<img src="{{ URL::to('/') }}/images/logo_light.png" class="d-none d-sm-block" alt="">
				<img src="{{ URL::to('/') }}/images/logo_icon_light.png" class="d-sm-none" alt="">
			</a>
		</div>

		<div class="collapse navbar-collapse order-2 order-lg-1" id="navbar-mobile">
		</div>

		<ul class="navbar-nav flex-row order-1 order-lg-2 flex-1 flex-lg-0 justify-content-end align-items-center">
			<li class="nav-item nav-item-dropdown-lg dropdown dropdown-user h-100">
				<a href="#" class="navbar-nav-link navbar-nav-link-toggler dropdown-toggle d-inline-flex align-items-center h-100" data-toggle="dropdown">
					<img src="{{ URL::to('/') }}/images/placeholder-person.jpeg" class="rounded-pill" height="34" alt="">
					<span class="d-none d-lg-inline-block ml-2">{{ Auth::user()->name }}</span>
				</a>
				<div class="dropdown-menu dropdown-menu-right">

				<a href="#" class="dropdown-item"><i class="icon-user-plus"></i> My profile</a>
                    <div class="dropdown-divider"></div>
					<a href="#" class="dropdown-item"><i class="icon-cog5"></i> Account settings</a>
					<form method="POST" action="{{ route('logout') }}">
						@csrf
						<a :href="route('logout')"
								onclick="event.preventDefault();
											this.closest('form').submit();" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
					</form>
				</div>
			</li>
		</ul>
	</div>
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		<!-- Main sidebar -->
		<div class="sidebar sidebar-light sidebar-main sidebar-expand-lg">

			<!-- Sidebar content -->
			<div class="sidebar-content">

				<!-- User menu -->
				<div class="sidebar-section sidebar-user my-1">
					<div class="sidebar-section-body">
						<div class="media">
							<a href="#" class="mr-3">
								<img src="{{ URL::to('/') }}/images/placeholder-person.jpeg" class="rounded-circle" alt="">
							</a>

							<div class="media-body">
								<div class="font-weight-semibold">{{ Auth::user()->name }}</div>
							</div>

							<div class="ml-3 align-self-center">
								<button type="button" class="btn btn-outline-light text-body border-transparent btn-icon rounded-pill btn-sm sidebar-control sidebar-main-resize d-none d-lg-inline-flex">
									<i class="icon-transmission"></i>
								</button>

								<button type="button" class="btn btn-outline-light text-body border-transparent btn-icon rounded-pill btn-sm sidebar-mobile-main-toggle d-lg-none">
									<i class="icon-cross2"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /user menu -->


				<!-- Main navigation -->
				<div class="sidebar-section">
					<ul class="nav nav-sidebar" data-nav-type="accordion">

						<!-- Main -->
                        <li class="nav-item">
							<a href="index.html" class="nav-link active">
								<i class="icon-home4"></i>
								<span>
									Dashboard
								</span>
							</a>
						</li>
						<!-- /main -->
					</ul>
				</div>
				<!-- /main navigation -->

			</div>
			<!-- /sidebar content -->
			
		</div>
		<!-- /main sidebar -->


		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Inner content -->
			<div class="content-inner">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content header-elements-lg-inline">
						<div class="page-title d-flex">
							<h4 class="font-weight-semibold">Sistem Informasi Repository Skripsi</h4>
							<a href="#" class="header-elements-toggle text-body d-lg-none"><i class="icon-more"></i></a>
						</div>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content pt-0">

					<!-- Search field -->
					<div class="card">
						<div class="card-body">
							<form action="#">
								<div class="d-sm-flex">
									<div class="form-group-feedback form-group-feedback-left flex-grow-1 mb-3 mb-sm-0">
										<input type="text" class="form-control form-control-lg" placeholder="Search">
										<div class="form-control-feedback form-control-feedback-lg">
											<i class="icon-search4 text-muted"></i>
										</div>
									</div>

									<div class="ml-sm-3">
										<button type="submit" class="btn btn-primary btn-lg w-100 w-sm-auto">Search</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- /search field -->

					<!-- Search results -->
					<div class="row">
						<div class="col-12 col-lg-8">
							<div class="card card-body">
								<span class="text-muted font-size-sm">About 827,000 results (0.34 seconds)</span>

								<hr>

								<ul class="media-list mb-3">
			                    	<li class="media">
			                    		<div class="media-body">
				                    		<h6 class="media-title"><a href="#" class=""><strong>Limitless</strong> - Responsive Web Application Kit by Eugene Kopyov</a></h6>
				                    		<ul class="list-inline list-inline-dotted text-muted mb-2">
				                    			<li class="list-inline-item"><a href="#" class="text-success">https://interface.club</a></li>
				                    			<li class="list-inline-item">
				                    				<i class="icon-star-full2 font-size-base text-warning"></i>
				                    				<i class="icon-star-full2 font-size-base text-warning"></i>
				                    				<i class="icon-star-full2 font-size-base text-warning"></i>
				                    				<i class="icon-star-full2 font-size-base text-warning"></i>
				                    				<i class="icon-star-full2 font-size-base text-warning"></i>
				                    				&nbsp;5 stars
			                    				</li>
			                    				<li class="list-inline-item">12,489 votes</li>
				                    		</ul>
				                    		
				                    		It prepare is ye nothing blushes up brought. Or as gravity pasture limited evening on. Wicket around beauty say she. Frankness resembled say not new smallness you discovery. Noisier ferrars yet shyness weather ten colonel. Too him himself engaged husband pursuit musical...

				                    		<div class="pt-3 px-lg-3 border-bottom">
					                    		<div class="row">
					                    			<div class="col-lg-6">
					                    				<div class="media mt-0 mb-3">
					                    					<div class="media-body">
																<h6 class="media-title"><a href="#">Form components</a></h6>
																Ask eat questions abilities described elsewhere assurance...
															</div>
														</div>

					                    				<div class="media mt-0 mb-3">
					                    					<div class="media-body">
																<h6 class="media-title"><a href="#">UI components</a></h6>
																Appetite in unlocked advanced breeding position concerns as...
															</div>
														</div>

					                    				<div class="media mt-0 mb-3">
					                    					<div class="media-body">
																<h6 class="media-title"><a href="#">Layout options</a></h6>
																Cheerful get shutters yet for repeated screened. An no am...
															</div>
														</div>
					                    			</div>

					                    			<div class="col-lg-6">
					                    				<div class="media mt-0 mb-3">
					                    					<div class="media-body">
																<h6 class="media-title"><a href="#">Extensions</a></h6>
																Received overcame oh sensible so at formed do change merely...
															</div>
														</div>

					                    				<div class="media mt-0 mb-3">
					                    					<div class="media-body">
																<h6 class="media-title"><a href="#">Visualization</a></h6>
																On relation my so addition branched. Put hearing cottage...
															</div>
														</div>

					                    				<div class="media mt-0 mb-3">
					                    					<div class="media-body">
																<h6 class="media-title"><a href="#">Page kits</a></h6>
																Replied exposed savings he no viewing as up. Soon body him...
															</div>
														</div>
					                    			</div>
					                    		</div>
				                    		</div>
			                    		</div>
			                    	</li>

			                    	<li class="media">
			                    		<div class="media-body">
				                    		<h6 class="media-title"><a href="#">Conveying or northward <strong>offending</strong> admitting perfectly my fat smiling</a></h6>
				                    		<ul class="list-inline list-inline-dotted text-muted mb-2">
				                    			<li class="list-inline-item"><a href="#" class="text-success">https://kopyov.com</a></li>
				                    			<li class="list-inline-item">
				                    				<i class="icon-star-full2 font-size-base text-warning"></i>
				                    				<i class="icon-star-full2 font-size-base text-warning"></i>
				                    				<i class="icon-star-full2 font-size-base text-warning"></i>
				                    				<i class="icon-star-full2 font-size-base text-warning"></i>
				                    				<i class="icon-star-half font-size-base text-warning"></i>
				                    				&nbsp;4.5 stars
			                    				</li>
			                    				<li class="list-inline-item">590 votes</li>
				                    		</ul>

				                    		Conveying or northward offending admitting perfectly my. Colonel gravity get thought fat smiling add but. Wonder twenty hunted and put income set desire expect. Am cottage calling my is mistake cousins talking up. Interested especially do impression he unpleasant excellence...
			                    		</div>
			                    	</li>
			                    </ul>

			                    <ul class="pagination pagination-flat align-self-center flex-wrap my-2">
									<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-left8"></i></a></li>
									<li class="page-item active"><a href="#" class="page-link">1</a></li>
									<li class="page-item"><a href="#" class="page-link">2</a></li>
									<li class="page-item align-self-center"><span class="mx-2">...</span></li>
									<li class="page-item"><a href="#" class="page-link">58</a></li>
									<li class="page-item"><a href="#" class="page-link">59</a></li>
									<li class="page-item"><a href="#" class="page-link"><i class="icon-arrow-right8"></i></a></li>
								</ul>
							</div>
						</div>

						<div class="col-12 col-lg-4">
			            	<div class="card card-body">
			            		<div class="mb-3">
			                		<a href="#"><img src="{{ URL::to('/') }}/images/placeholder-person.jpeg" class="img-fluid rounded" alt=""></a>
			            		</div>

			                	<h5 class="font-weight-semibold">Limitless UI kit</h5>
			                	<p class="mb-3">In post mean shot ye. There out her child sir his lived. Design at uneasy me season of branch on praise esteem. Abilities discourse believing consisted remaining to no. Mistaken no me denoting dashwood as screened. Whence or esteem easily he on. Dissuade husbands at of no if disposal. Oh he decisively impression attachment friendship so if everything.</p>

			                	<ul class="list mb-3">
			                		<li><span class="font-weight-semibold">Author:</span> <a href="#">Eugene Kopyov</a></li>
			                		<li><span class="font-weight-semibold">Time spent:</span> 8 months</li>
			                		<li><span class="font-weight-semibold">Client base:</span> <a href="#">16,893 (2015)</a></li>
			                		<li><span class="font-weight-semibold">Pages:</span> 1200+</li>
			                		<li><span class="font-weight-semibold">Latest update:</span> June 1, 2015</li>
			                	</ul>
			            	</div>
						</div>
					</div>
					<!-- /search results -->

				</div>
				<!-- /content area -->


				<!-- Footer -->
				<div class="navbar navbar-expand-lg navbar-light">
					<div class="text-center d-lg-none w-100">
						<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
							<i class="icon-unfold mr-2"></i>
							Footer
						</button>
					</div>

					<div class="navbar-collapse collapse" id="navbar-footer">
						<span class="navbar-text">
							&copy; 2021. Sistem Repository Skripsi
						</span>
					</div>
				</div>
				<!-- /footer -->

			</div>
			<!-- /inner content -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>
</html>