<x-app-layout>
<div class="card">
    <div class="card-header">
        <h5 class="card-title">Upload Detail Skripsi</h5>
    </div>

    <div class="card-body">
        <form action="{{ route('member.document.store') }}"  method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Title:</label>
                <input type="text" id="titleSkripsi" name="titleskripsi" class="form-control" placeholder="Masukkan Judul Skripsi Anda">
            </div>

            <div class="form-group">
                <label>NIM:</label>
                <input type="text" id="nim" name="nim" class="form-control" placeholder="Masukkan Nim Mahasiswa">
            </div>

            <div class="form-group">
                <label>Dosen 1 :</label>
                <select id="dosen1" class="custom-select" name="dosen1">
                    <option disabled>Pilih Dosen Pembimbing 1</option>
                    <option value="sbj">Bpk. Subarjo</option>
                    <option value="spr">Bpk. Supratman</option>
                    <option value="sdr">Bpk. Sudiro</option>
                </select>
            </div>

            <div class="form-group">
                <label>Dosen 2 :</label>
                <select id="dosen2" class="custom-select" name="dosen2">
                    <option disabled>Pilih Dosen Pembimbing 2</option>
                    <option value="sbj">Bpk. Subarjo</option>
                    <option value="spr">Bpk. Supratman</option>
                    <option value="sdr">Bpk. Sudiro</option>
                </select>
            </div>

            <div class="form-group">
                <label>Tahun Terbit :</label>
                <input type="text" id="thn" name="thn" class="form-control" placeholder="Tahun Terbit / Approval Skripsi">
            </div>

            <div class="form-group">
                <label>Upload Cover Skripsi :</label>
                <label class="custom-file">
                    <input type="file" id="skripsiCover" name="skripsicover" class="custom-file-input">
                    <span class="custom-file-label">Choose file</span>
                </label>
            </div>

            <div class="form-group">
                <label>Upload Softfile Skripsi :</label>
                <label class="custom-file">
                    <input type="file" id="skripsiSoftfile" name="skripsisoftfile" class="custom-file-input">
                    <span class="custom-file-label">Choose file</span>
                </label>
            </div>

            <div class="form-group">
                <label>Abstract :</label>
                <textarea rows="5" cols="5" id="abstract" name="abstract" class="form-control" placeholder="Masukan abstraksi / Deskripsi singkat dari judul skripsi"></textarea>
            </div>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i></button>
            </div>
        </form>
    </div>
</div>
</x-app-layout>
