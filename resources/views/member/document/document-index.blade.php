<x-app-layout>
<div class="card">
						<div class="card-header">
							<h5 class="card-title">List Document</h5>
						</div>

						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>User</th>
										<th>Nim</th>
										<th>Title</th>
										<th>Abstract</th>
										<th>Dosen 1</th>
										<th>Dosen 2</th>
										<th>Tahun Terbit</th>
										<th>Image</th>
										<th>Attachment</th>
									</tr>
								</thead>
								<tbody>
                                    @foreach($documents as $value)
									<tr>
										<td>1</td>
										<td>{{ $value->member->firstname . " " . $value->member->lastname }}</td>
										<td>{{ $value->member->nim }}</td>
										<td>{{ $value->title }}</td>
										<td>{{ $value->abstract }}</td>
										<td>{{ $value->dosen_pembimbing_1 }}</td>
										<td>{{ $value->dosen_pembimbing_2 }}</td>
										<td>{{ $value->publish_year }}</td>
										<td>{{ $value->image }}</td>
										<td>{{ $value->attachment }}</td>
									</tr>
                                    @endforeach
								</tbody>
							</table>
						</div>

                        @if ($documents->lastPage() > 1)
                        <ul class="pagination pagination-flat align-self-center flex-wrap my-2">
                            <li class="page-item {{ ($documents->currentPage() == 1) ? ' disabled' : '' }}">
                                <a href="{{ $documents->url(1) }}" class="page-link"><i class="icon-arrow-left8"></i></a>
                            </li>
                            @for ($i = 1; $i <= $documents->lastPage(); $i++)
                                <li class="page-item {{ ($documents->currentPage() == $i) ? ' active' : '' }}">
                                    <a href="{{ $documents->url($i) }}" class="page-link">{{ $i }}</a>
                                </li>
                            @endfor
                            <li class="page-item {{ ($documents->currentPage() == $documents->lastPage()) ? ' disabled' : '' }}">
                                <a href="{{ $documents->url($documents->currentPage()+1) }}" class="page-link"><i class="icon-arrow-right8"></i></a>
                            </li>
                        </ul>
                        @endif
					</div>
</x-app-layout>
