<?php


namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Models\Document;
use Auth;
use DB;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'documents' => Document::paginate(10)
        ];

        return view('member.document.document-index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('member.document.document-upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {

        $req->validate([
            'titleskripsi' => 'required',
            'dosen1' => 'required',
            'dosen2' => 'required',
            'thn' => 'required',
            'skripsicover' => 'required',
            'skripsisoftfile' => 'required',
            'abstract' => 'required',
            ]);
    
            $fileModel = new Document;
    
            // if($req->file()) {
                
                $fileModel->user_id = Auth::user()->id;
                $fileModel->nim = $req->nim;
                $fileModel->title = $req->titleskripsi;
                $fileModel->abstract = $req->abstract;
                $fileModel->dosen_pembimbing_1 = $req->dosen1;
                $fileModel->dosen_pembimbing_2 = $req->dosen2;
                $fileModel->publish_year = $req->thn;;
                
                DB::beginTransaction();

                try{
                    $fileName = time().'_'.$req->file('skripsisoftfile')->getClientOriginalName();
                    $req->file('skripsisoftfile')->move(public_path('uploads'), $fileName);
                    $fileModel->attachment = time().'_'.$req->file('skripsisoftfile')->getClientOriginalName();
                    
                    $fileName2 = time().'_'.$req->file('skripsicover')->getClientOriginalName();
                    $req->file('skripsicover')->move(public_path('uploads'), $fileName2);
                    $fileModel->image = time().'_'.$req->file('skripsicover')->getClientOriginalName();
                    
                    $fileModel->save();
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                    dd($e);
                }

                return back()
                ->with('success','File has been uploaded.')
                ->with('file', $fileName2);
            // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
