<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Document;

class SiteController extends Controller
{
    public function search(Request $req)
    {
        $data = [
            'documents' => Document::where('title', 'like', '%' . $req->input('q') . '%')->paginate(10)
        ];

        return view('site.document-search')->with($data);
    }

    public function search_data(Request $req)
    {

        $documents = Document::where('title', 'like', '%' . $req->input('q') . '%')->paginate(10);

        if($req->ajax()) {
            return response()->json($documents);
        }

		return view('site.document-search')->with($documents);

    }
    
    public function detail($id)
    {
        $data = [
            'document' => Document::where('id', $id)->first()
        ];

        return view('site.document-detail')->with($data);
    }
}
