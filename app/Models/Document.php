<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'nim',
        'title',
        'abstract',
        'dosen_pembimbing_1',
        'dosen_pembimbing_2',
        'publish_year',
        'image',
        'attachment',
    ];

    public function user(){
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function member(){
        return $this->hasOne('App\Models\Member', 'user_id', 'user_id');
    }

}
