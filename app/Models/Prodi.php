<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'prodi';

    protected $fillable = [
        'id',
        'fakultas_id',
        'name',
    ];

    public function fakultas(){
        return $this->hasOne('App\Models\Fakultas', 'id', 'fakultas_id');
    }
}
