<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@search')->name('site.document.index');
Route::get('/detail/{id}', 'SiteController@detail')->name('site.document.detail');
Route::get('/search/data', 'SiteController@search_data')->name('site.document.data');
Route::get('/dashboard', 'UserController@index')->name('dashboard');
Route::get('/profile', 'ProfileController@index')->name('profile.index');

Route::group(['as'=>'admin.','prefix' => 'admin', 'middleware'=>['auth','admin']], function () {
    Route::get('member', 'Admin\MemberController@index')->name('member.index');
});

Route::group(['as'=>'member.','prefix' => 'member', 'middleware'=>['auth','member']], function () {
    Route::get('document', 'Member\DocumentController@index')->name('document.index');
    Route::get('document/upload', 'Member\DocumentController@create')->name('document.upload');

    Route::post('document/upload', 'Member\DocumentController@store')->name('document.store');
});

require __DIR__.'/auth.php';
