<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DocumentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('documents')->truncate();

        DB::table('documents')->insert([
            'id' => '1',
            'user_id' => '2',
            'nim' => '0618104044',
            'title' => 'IMPLEMENTASI DATA MINING PADA PERUSAHAN XYZ',
            'abstract' => 'Abstraksi - Ini menampilkan abstraksi dari skripsi yang dipilih sebagai preview user sebelum mengakses skripsi lebih jauh.',
            'dosen_pembimbing_1' => 'Bpk. Subarjo',
            'dosen_pembimbing_2' => 'Bpk. Supratman',
            'publish_year' => '2021',
            'image' => 'image.jpg',
            'attachment' => 'document.pdf',
        ]);

    }
}
