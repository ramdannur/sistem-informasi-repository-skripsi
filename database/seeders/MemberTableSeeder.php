<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class MemberTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('members')->truncate();

        DB::table('members')->insert([
            'id' => '1',
            'user_id' => '2',
            'nim' => '0618104044',
            'firstname' => 'Ramdan',
            'lastname' => 'Nurul',
            'birthdate' => '1998-01-02',
            'tahun_angkatan' => '2018',
            'prodi_id' => '1',
        ]);

        // \App\Models\User::factory(10)->member()->has(\App\Models\Member::factory()->count(1))->has(\App\Models\Document::factory()->count(2))->create();
    }
}
