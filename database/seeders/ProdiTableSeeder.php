<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ProdiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prodi')->truncate();

        DB::table('prodi')->insert([
            'id' => '1',
            'fakultas_id' => '1',
            'name' => 'Teknik Informatika',
        ]);

        DB::table('prodi')->insert([
            'id' => '2',
            'fakultas_id' => '1',
            'name' => 'Sistem Informasi',
        ]);

        DB::table('prodi')->insert([
            'id' => '3',
            'fakultas_id' => '2',
            'name' => 'Manajemen Akuntansi',
        ]);
    }
}
