<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class FakultasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fakultas')->truncate();

        DB::table('fakultas')->insert([
            'id' => '1',
            'name' => 'Fakultas Teknik',
        ]);

        DB::table('fakultas')->insert([
            'id' => '2',
            'name' => 'Fakultas Manajemen',
        ]);
    }
}
