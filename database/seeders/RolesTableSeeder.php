<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role')->truncate();
        
        DB::table('role')->insert([
            'id' => '1',
            'name' => 'admin',
        ]);

        DB::table('role')->insert([
            'id' => '2',
            'name' => 'Member',
        ]);
    }
}
