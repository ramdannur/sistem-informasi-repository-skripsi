<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integer('id', true)->length(11);
            $table->string('name')->length(50);
            $table->string('password');
            $table->string('email')->length(50);
            $table->integer('role_id')->length(3)->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->integer('created_by')->length(11)->nullable();
            $table->integer('updated_by')->length(11)->nullable();
            
            $table->foreign('role_id')->references('id')->on('role')
            ->onUpdate('set null')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
