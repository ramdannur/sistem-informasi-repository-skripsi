<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->integer('id', true)->length(6);
            $table->integer('user_id')->length(11)->nullable();
            $table->string('nim')->length(10);
            $table->string('firstname')->length(50);
            $table->string('lastname')->length(50);
            $table->date('birthdate')->nullable();;
            $table->integer('tahun_angkatan')->length(4)->nullable();
            $table->integer('prodi_id')->length(3)->nullable();
            $table->timestamps();
            $table->integer('created_by')->length(11)->nullable();
            $table->integer('updated_by')->length(11)->nullable();

            $table->foreign('prodi_id')->references('id')->on('prodi')
            ->onUpdate('set null')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
