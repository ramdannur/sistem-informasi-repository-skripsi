<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->integer('id', true)->length(11);
            $table->integer('user_id')->length(11)->nullable();
            $table->string('nim')->length(10);
            $table->text('title');
            $table->text('abstract');
            $table->string('dosen_pembimbing_1')->length(100);
            $table->string('dosen_pembimbing_2')->length(100);
            $table->integer('publish_year')->length(4);
            $table->string('image')->length(100);
            $table->string('attachment')->length(255);
            $table->timestamps();
            $table->integer('created_by')->length(11)->nullable();
            $table->integer('updated_by')->length(11)->nullable();

            $table->foreign('user_id')->references('id')->on('users')
            ->onUpdate('set null')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
