<?php

namespace Database\Factories;

use App\Models\Document;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class DocumentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Document::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $filePath = public_path('uploads');

        return [
            'nim' => "06181" . $this->faker->numberBetween(1,9999),
            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'abstract' => $this->faker->text($maxNbChars = 500),
            'dosen_pembimbing_1' => $this->faker->titleMale() . " " . $this->faker->name(),
            'dosen_pembimbing_2' => $this->faker->titleFemale() . " " . $this->faker->name(),
            'publish_year' => $this->faker->numberBetween(2010,2021),
            'image' => $this->faker->image($filePath,300,400, null, false),
            'attachment' => $this->faker->image($filePath, 600, 600, null, false),
        ];
    }
}
