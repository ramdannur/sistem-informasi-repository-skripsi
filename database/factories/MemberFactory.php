<?php

namespace Database\Factories;

use App\Models\Member;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class MemberFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Member::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nim' => "06181" . $this->faker->numberBetween(1,9999),
            'firstname' => $this->faker->firstName(),
            'lastname' => $this->faker->lastname(),
            'birthdate' => $this->faker->dateTime(),
            'tahun_angkatan' => $this->faker->numberBetween(2010,2021),
            'prodi_id' => $this->faker->numberBetween(1,3),
        ];
    }
}
